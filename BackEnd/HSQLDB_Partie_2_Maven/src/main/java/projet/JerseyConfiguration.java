package projet;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import projet.model.CompteRessource;
import projet.model.EvenementRessource;

import org.glassfish.jersey.servlet.ServletProperties;


@Component  //. Il permet à cette classe d'être enregistrée pendant que Spring Boot analyse automatiquement les classes Java dans le dossier source.
@ApplicationPath("rest")
@Configuration
public class JerseyConfiguration extends ResourceConfig  //ResourceConfig fournit des capacités avancées pour simplifier l'enregistrement des composants
{
	public JerseyConfiguration()
	{
		register(CorsFilter.class);
		register(EvenementRessource.class);
		register(CompteRessource.class);
        property(ServletProperties.FILTER_FORWARD_ON_404, true);
		
	}

}
