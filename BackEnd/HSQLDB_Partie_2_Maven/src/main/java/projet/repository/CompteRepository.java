package projet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import projet.model.Compte;
import projet.model.Evenement;


public interface CompteRepository extends CrudRepository<Compte, Long> {
	List<Compte> findByPseudo(String Pseudo);

	List<Compte> findByEmail(String email);

	Compte findByToken(Integer token);
}
