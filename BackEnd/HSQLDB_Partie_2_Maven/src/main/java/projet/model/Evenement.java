package projet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Evenement implements Serializable {
	/*
	 * Permet de creer un evenment avec un titre, une description, une date de début et de fin
	 */
	private static final long serialVersionUID = -8537962680206576813L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String description;
	private Date debut;
	private Date fin;
	private ArrayList<String> list_Name;
	private String chef;
	
	public Evenement() {
		// TODO Auto-generated constructor stub
	}
	/*
	 * Constructeur 
	 */
	public Evenement(String chef,String name, String description, Date debut, Date fin) {
		this.chef = chef;
		this.name = name;
		this.description = description;
		this.debut = debut;
		this.fin = fin;
		list_Name = new ArrayList<String>();
	}
	/*
	 * Fonction qui permet l'accés au donné et la modification
	 */
	public String getChef() {
		return chef;
	}
	public void setChef(String chef) {
		this.chef = chef;
	}
	public void addEvenement(String e) {
		list_Name.add(e);
	}
	public String getElementOflist_Name(int i) {
		return list_Name.get(i);
	}
	public ArrayList<String> getlist_Name() {
		return list_Name;
	}
	public void setList_E(ArrayList<String> list_Name) {
		this.list_Name = list_Name;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDebut() {
		return debut;
	}

	public void setDebut(Date debut) {
		this.debut = debut;
	}

	public Date getFin() {
		return fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	}

	@Override
	public String toString() {
		return "Evenement [name=" + name + ", description=" + description + ", debut=" + debut + ", fin=" + fin + "]";
	}
	
	
	
}
