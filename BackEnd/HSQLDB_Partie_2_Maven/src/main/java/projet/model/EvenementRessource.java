package projet.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import projet.repository.EvenementRepository;
/*
 * fichier qui reference tout les url pour accéde au information d'un compte 
 */

@Path("/Evenement")
public class EvenementRessource {
	
	@Autowired
	private EvenementRepository evenementrepository;		//Permet de faire des requêtes grâce a la classe mére crudrepository 
	
	public EvenementRessource() {}
	
	//Création d'un nouveau Evenement dans la base. Il ne peut pas avoir de doublon
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/eve/create")
	public Evenement createEvenement(Evenement c) {
		List<Evenement> temp =(List<Evenement>) evenementrepository.findByName(c.getName());
		if(temp.isEmpty()) {
			return evenementrepository.save(c);
		}
				return null;
	}
	
	//Retourne tout les Evenement de la base
	@GET
	@Produces("application/json")
	@Path("/eve/readAll")
	public List<Evenement> readAll(){
		return (List<Evenement>) evenementrepository.findAll();
	}
	
	//Retourne un Evenement par son name
	@GET
	@Produces("application/json")
	@Path("/eve/searchEvenementByName")
	public List<Evenement>  readEvenementByName(@QueryParam("name") String name) {
		return (List<Evenement>) evenementrepository.findByName(name);
	}
	
	//Retourne un Evenement par sa date de commencement
	@GET
	@Produces("application/json")
	@Path("/eve/searchEvenementByDebut")
	public List<Evenement> readEvenementByDebut(@QueryParam("debut") String debut) throws ParseException {
		System.out.println(debut);
		 SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
	        Date date = parser.parse(debut);
		return (List<Evenement>) evenementrepository.findByDebut(date);
	}
	
	//Retourne un Evenement par sa date de fin
	@GET
	@Produces("application/json")
	@Path("/eve/searchEvenementByFin")
	public List<Evenement> readEvenementByFin(@QueryParam("fin") Date fin) {
		return (List<Evenement>) evenementrepository.findByFin(fin);
	}
	
	//Suprime un Evenement par son name
	@DELETE
	@Path("/eve/deleteEvenementByName/{name}")
	public void  deleteEvenementByName(@PathParam("name") String name) {
		evenementrepository.deleteByName(name);
	}
	
	//fonction qui ajoute un participant a la liste de l'evenement mais non utiliser
	@POST
	@Path("/eve/updateEvenementparticipe")
	public void  updateEvenementparticipe(@QueryParam("name") String name, @QueryParam("participe") String participe) {
		Evenement e= (Evenement) evenementrepository.findByName(name);
		e.addEvenement(participe);
		System.out.println(participe);
	}
}
