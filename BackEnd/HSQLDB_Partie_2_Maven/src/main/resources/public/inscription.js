//js de inscription 
//grace a ce fichier je peux m'inscrire 
const getUrl = (path = "/") => {
		const protocol = "http";
		const address = "localhost";
		const port = 8080;
		return protocol + '://' + address + ':' + port + '' + path;
	};
	
	$(document).ready(function () {
		
	//click formulaire d'inscription
		$("#creation").click(function () {
				var pseudo = $("#pseudo").val();
				var email = $("#email").val();
				var naissance = $("#naissance").val();
				var mdp = $("#mdp").val();
				$.ajax({
					type: "post",
					url: getUrl("/rest/Compte/cmpt/create"),
					contentType: "application/json; charset=UTF-8",
					data: JSON.stringify({
						pseudo:pseudo	,
						email:email,
						naissance:naissance,
						mdp:mdp
					}),
					dataType: "json",
					success: function (result) {
					// renvoi sur connection apres la creation du compte 
					window.location = "./connection.html";
					
					}
				});
			});
		});
